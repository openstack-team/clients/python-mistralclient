python-mistralclient (1:5.4.0-1) experimental; urgency=medium

  * New upstream release.
  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Mar 2025 11:46:56 +0100

python-mistralclient (1:5.3.0-4) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090533).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 09:57:53 +0100

python-mistralclient (1:5.3.0-3) unstable; urgency=medium

  * Removed python3-six from (build-)depends (Closes: #1083028).

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Oct 2024 22:48:46 +0200

python-mistralclient (1:5.3.0-2) unstable; urgency=medium

  * Removed /usr/bin alternative removal in {pre,post}rm.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 10:22:04 +0200

python-mistralclient (1:5.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Sep 2024 11:18:56 +0200

python-mistralclient (1:5.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Apr 2024 08:28:57 +0200

python-mistralclient (1:5.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Mar 2024 17:22:56 +0100

python-mistralclient (1:5.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 15:44:10 +0200

python-mistralclient (1:5.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Sep 2023 17:17:46 +0200

python-mistralclient (1:5.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1046347).
  * Correctly install the doc in
    debian/python3-mistralclient/usr/share/doc/python3-mistralclient/html.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Aug 2023 23:54:18 +0200

python-mistralclient (1:5.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 12:22:51 +0200

python-mistralclient (1:5.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 14:13:23 +0100

python-mistralclient (1:4.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:33:09 +0200

python-mistralclient (1:4.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Sep 2022 13:59:33 +0200

python-mistralclient (1:4.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 11:11:23 +0100

python-mistralclient (1:4.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Feb 2022 21:37:44 +0100

python-mistralclient (1:4.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:47:37 +0200

python-mistralclient (1:4.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Sep 2021 09:11:30 +0200

python-mistralclient (1:4.2.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 11:19:28 +0200

python-mistralclient (1:4.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Mar 2021 09:59:47 +0100

python-mistralclient (1:4.1.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 16:21:08 +0200

python-mistralclient (1:4.1.1-1) experimental; urgency=medium

  * Fixed Homepage field and watch file.
  * New upstream release.
  * Add python3-os-client-config as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 10:29:45 +0200

python-mistralclient (1:4.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 23:19:19 +0200

python-mistralclient (1:4.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-coverage as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 09 Apr 2020 21:39:02 +0200

python-mistralclient (1:3.10.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 01:36:16 +0200

python-mistralclient (1:3.10.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 00:03:29 +0200

python-mistralclient (1:3.8.0-5) unstable; urgency=medium

  * Really removed Python 2 build-dependencies (Closes: #937922).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 21:56:33 +0200

python-mistralclient (1:3.8.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * Removed Python 2 build-dependencies (Closes: #937922).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 21:53:06 +0200

python-mistralclient (1:3.8.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 13:31:15 +0200

python-mistralclient (1:3.8.0-2) experimental; urgency=medium

  * Removed management of alternatives.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Mar 2019 14:45:52 +0100

python-mistralclient (1:3.8.0-1) experimental; urgency=medium

  * New upstream release.
  * Remove Python 2 support.
  * Run tests with installed Python module.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Mar 2019 09:15:22 +0100

python-mistralclient (1:3.7.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Blacklist HTTPClientTest.test_get_request_options_with_profile_enabled() in
    Python 3.x.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 00:08:49 +0200

python-mistralclient (1:3.7.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Building doc with Python 3.
  * Using pkgos-dh_auto_test.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 11:12:32 +0200

python-mistralclient (1:3.3.0-1) unstable; urgency=medium

  [ Michal Arbet ]
  * New upstream version

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jun 2018 12:43:39 +0000

python-mistralclient (1:3.2.0-2) unstable; urgency=medium

  * Python 3 has now priority over Python 2.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Mar 2018 13:10:50 +0100

python-mistralclient (1:3.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Feb 2018 00:38:34 +0000

python-mistralclient (1:3.1.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 00:07:55 +0000

python-mistralclient (1:3.1.3-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Removed UPSTREAM_GIT, changed to default value
  * d/copyright: Changed source URL to new one

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_install.
  * Switching to python -m nose.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Oct 2017 22:08:23 +0200

python-mistralclient (1:2.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 13:33:59 +0000

python-mistralclient (1:2.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 13:15:51 +0000

python-mistralclient (1:2.0.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Corey Bryant ]
  * New upstream release.
  * d/control: Align (Build-)Depends with upsream.

  [ Thomas Goirand ]
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Mar 2016 12:33:09 +0100

python-mistralclient (1:1.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Jan 2016 10:24:42 +0000

python-mistralclient (1:1.1.0-4) unstable; urgency=medium

  * override_dh_python3 to fix Py3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 23:17:16 +0000

python-mistralclient (1:1.1.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 22:07:40 +0000

python-mistralclient (1:1.1.0-2) experimental; urgency=medium

  * Added Python 3 support.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2015 12:32:29 +0000

python-mistralclient (1:1.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2015 12:27:55 +0000

python-mistralclient (1:1.0.2-1) experimental; urgency=medium

  * New upstream release.
  * Applied Ubuntu patch (Closes: #796462).
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Sep 2015 10:12:57 +0000

python-mistralclient (1:1.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 14:08:47 +0000

python-mistralclient (2015.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 May 2015 20:11:37 +0000

python-mistralclient (2015.1~rc1-1) experimental; urgency=medium

  * Initial release. (Closes: #781149).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Feb 2015 19:36:19 +0100
